package net.chenlin.dp.base.controller;

import net.chenlin.dp.common.annotation.SysLog;
import net.chenlin.dp.common.controller.AbstractController;
import net.chenlin.dp.common.entity.R;
import net.chenlin.dp.common.utils.CommonUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/base/hello")
public class HelloController extends AbstractController {

    @SysLog("新增测试方法")//面向切面编程AOP,面向切面编程AOP
    @RequestMapping("/test")
    public R test(){
        return CommonUtils.msg(1);
    }

}
